package hr.vub;

public class Main {

    public static void print(){
        for(int i = 0; i < 100; i++){
            System.out.println(i);

        }
    }

    public static void main(String[] args) {

        System.out.println("Hello World!");
        Main.print();
        Dummy dummy = new Dummy();
        dummy.print();
        Circle circle = new Circle(10);
        circle.setRadius(5);
        System.out.println("Current radius: " + circle.getRadius());
        System.out.println("Current area: " + circle.getArea());
        System.out.println("Current circumference: " + circle.getCircumference());
        Sphere sphere = new Sphere(circle.radius);
        System.out.println("Current volume:" + sphere.getVolume());

    }


}




