package hr.vub;

public class Sphere extends Circle{
    public Sphere(double radius) {
        super(radius);
    }

    public double getVolume(){
        return (4.0/3.0)* Math.PI * Math.pow(radius,3);
    }
}
